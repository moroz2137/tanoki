# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :tanoki,
  ecto_repos: [Tanoki.Repo]

# Configures the endpoint
config :tanoki, TanokiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "XUU8+i6a2CehpnjMLK5ubHrklHN0D7nzgjLMaJ2/vYmxLQKz3Fw/sj25NytiRuP6",
  render_errors: [view: TanokiWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Tanoki.PubSub,
  live_view: [signing_salt: "cUUf7hVi"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
