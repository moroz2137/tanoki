defmodule Tanoki.Catalogue.Product do
  use Ecto.Schema
  import Ecto.Changeset

  schema "products" do
    field :base_price, :decimal
    field :description, :string
    field :name, :string
    field :slug, :string

    timestamps()
  end

  @doc false
  def changeset(product, attrs) do
    product
    |> cast(attrs, [:name, :slug, :description, :base_price])
    |> validate_required([:name, :slug, :description, :base_price])
    |> unique_constraint(:slug)
  end
end
