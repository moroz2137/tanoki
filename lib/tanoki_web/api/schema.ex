defmodule TanokiWeb.Api.Schema do
  use Absinthe.Schema
  import TanokiWeb.Api.Macros

  import_types(Absinthe.Type.Custom)

  object :product do
    field :id, non_null(:id)
    field :base_price, non_null(:decimal)
    field :description, :string
    field :name, non_null(:string)
    field :slug, non_null(:string)
    timestamps()
  end

  query do
    field :product, :product do
      arg(:id, non_null(:id))
    end
  end
end
