defmodule Tanoki.CatalogueTest do
  use Tanoki.DataCase

  alias Tanoki.Catalogue

  describe "products" do
    alias Tanoki.Catalogue.Product

    @valid_attrs %{base_price: "120.5", description: "some description", name: "some name", slug: "some slug"}
    @update_attrs %{base_price: "456.7", description: "some updated description", name: "some updated name", slug: "some updated slug"}
    @invalid_attrs %{base_price: nil, description: nil, name: nil, slug: nil}

    def product_fixture(attrs \\ %{}) do
      {:ok, product} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Catalogue.create_product()

      product
    end

    test "list_products/0 returns all products" do
      product = product_fixture()
      assert Catalogue.list_products() == [product]
    end

    test "get_product!/1 returns the product with given id" do
      product = product_fixture()
      assert Catalogue.get_product!(product.id) == product
    end

    test "create_product/1 with valid data creates a product" do
      assert {:ok, %Product{} = product} = Catalogue.create_product(@valid_attrs)
      assert product.base_price == Decimal.new("120.5")
      assert product.description == "some description"
      assert product.name == "some name"
      assert product.slug == "some slug"
    end

    test "create_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Catalogue.create_product(@invalid_attrs)
    end

    test "update_product/2 with valid data updates the product" do
      product = product_fixture()
      assert {:ok, %Product{} = product} = Catalogue.update_product(product, @update_attrs)
      assert product.base_price == Decimal.new("456.7")
      assert product.description == "some updated description"
      assert product.name == "some updated name"
      assert product.slug == "some updated slug"
    end

    test "update_product/2 with invalid data returns error changeset" do
      product = product_fixture()
      assert {:error, %Ecto.Changeset{}} = Catalogue.update_product(product, @invalid_attrs)
      assert product == Catalogue.get_product!(product.id)
    end

    test "delete_product/1 deletes the product" do
      product = product_fixture()
      assert {:ok, %Product{}} = Catalogue.delete_product(product)
      assert_raise Ecto.NoResultsError, fn -> Catalogue.get_product!(product.id) end
    end

    test "change_product/1 returns a product changeset" do
      product = product_fixture()
      assert %Ecto.Changeset{} = Catalogue.change_product(product)
    end
  end
end
